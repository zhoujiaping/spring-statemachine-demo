package com.example.msg.model;

import com.example.msg.state.MsgStates;

public class Msg {

    public Integer id;
    public MsgStates state;
    public String content;
    public Msg(Integer id, MsgStates state, String content){
        this.id = id;
        this.state = state;
        this.content = content;
    }
    @Override
    public String toString(){
        return "Msg(id="+id+",state="+state+",content="+content+")";
    }
}
