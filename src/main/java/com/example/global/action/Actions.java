package com.example.global.action;

import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

/**
 * 动作监听
 */
@WithStateMachine(name="myStateMachine")
public class Actions {

    @OnTransition(target = "S1")
    public void createMsg() {
        println("创建消息");
    }

    @OnTransition(target = "S2")
    public void sendMsg() {
        println("发送消息");
    }

    @OnTransition(target = "S3")
    public void readMsg() {
        println("读取消息");
    }

    private void println(String msg){
        System.out.println(msg);
    }

}