package com.example.msg.cmd;

import com.example.msg.repo.MsgRepo;
import com.example.msg.service.MsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MsgCommandRunner implements CommandLineRunner {

    @Autowired
    private MsgService msgService;

    @Autowired
    private MsgRepo msgRepo;

    @Override
    public void run(String... args) throws Exception {
        msgService.send(2);
        msgService.create("kitty");
        msgService.send(3);
        msgService.create("woo");
        System.out.println(msgRepo.getMsgMap());
        //{1=Msg(id=1,state=INIT,content=hello), 2=Msg(id=2,state=SENT,content=hi), 3=Msg(id=3,state=SENT,content=kitty), 4=Msg(id=4,state=INIT,content=woo)}
    }
}
