# 一、什么是沙箱
Java安全模型的核心就是Java沙箱（sandbox），什么是沙箱？
沙箱是一个限制程序运行的环境。限制程序运行一方面是为了保护系统资源，同时另一方面也为了保护程序自己。
沙箱主要限制系统资源访问，那系统资源包括什么？——CPU、内存、文件系统、网络。
不同级别的沙箱对这些资源访问的限制也可以不一样。
# 二、Java中的安全模型
当前最新的安全机制实现，引入了域 (Domain) 的概念。
虚拟机会把所有代码加载到不同的系统域和应用域，系统域部分专门负责与关键资源进行交互，
而各个应用域部分则通过系统域的部分代理来对各种需要的资源进行访问。
虚拟机中不同的受保护域 (Protected Domain)，对应不一样的权限 (Permission)。
存在于不同域中的类文件就具有了当前域的全部权限，如下图所示

# 三、沙箱的基本组件
## 3.1 字节码校验器（bytecode verifier）
确保Java类文件遵循Java语言规范。这样可以帮助Java程序实现内存保护。
但并不是所有的类文件都会经过字节码校验，比如核心类。
## 3.2 类装载器（class loader）
其中类装载器在3个方面对Java沙箱起作用
* 它防止恶意代码去干涉善意的代码；
* 它守护了被信任的类库边界；
* 它将代码归入保护域，确定了代码可以进行哪些操作。
虚拟机为不同的类加载器载入的类提供不同的命名空间，命名空间由一系列唯一的名称组成，每一个被装载的类将有一个名字，这个命名空间是由Java虚拟机为每一个类装载器维护的，它们互相之间甚至不可见。
类装载器采用的机制是双亲委派模式。
* 从最内层JVM自带类加载器开始加载，外层恶意同名类得不到加载从而无法使用；
* 由于严格通过包来区分了访问域，外层恶意的类通过内置代码也无法获得权限访问到内层类，破坏代码就自然无法生效。
## 3.3 存取控制器（access controller）
存取控制器可以控制核心API对操作系统的存取权限，而这个控制的策略设定，可以由用户指定。
## 3.4 安全管理器（security manager）
是核心API和操作系统之间的主要接口。实现权限控制，比存取控制器优先级高。
## 3.5 安全软件包（security package）
java.security下的类和扩展包下的类，允许用户为自己的应用增加新的安全特性。
# 四、沙箱包含的要素
## 4.1 权限
权限是指允许代码执行的操作。包含三部分：权限类型、权限名和允许的操作。权限类型是实现了权限的Java类名，是必需的。权限名一般就是对哪类资源进行操作的资源定位（比如一个文件名或者通配符、网络主机等），一般基于权限类型来设置，有的比如java.security.AllPermission不需要权限名。允许的操作也和权限类型对应，指定了对目标可以执行的操作行为，比如读、写等。如下面的例子：
```
permission java.security.AllPermission;    //权限类型
permission java.lang.RuntimePermission "stopThread";    //权限类型+权限名
permission java.io.FilePermission "/tmp/foo" "read";    //权限类型+权限名+允许的操作
```
## 4.2 代码源
代码源是类所在的位置，表示为以URL地址。
## 4.3 保护域
保护域用来组合代码源和权限，这是沙箱的基本概念。保护域就在于声明了比如由代码A可以做权限B这样的事情。
## 4.4 策略文件
策略文件是控制沙箱的管理要素，一个策略文件包含一个或多个保护域的项。
策略文件完成了代码权限的指定任务，策略文件包括全局和用户专属两种。
为了管理沙箱，策略文件我认为是最重要的内容。
JVM可以使用多个策略文件，不过一般两个最常用。
一个是全局的：$JREHOME/lib/security/java.policy，作用于JVM的所有实例。
另一个是用户自己的，可以存储到用户的主目录下。
策略文件可以使用jdk自带的policytool工具编辑。
策略文件只支持白名单模式，即只能指定谁有哪些权限，不能指定谁不能有哪些权限。
## 4.5 密钥库
保存密钥证书的地方。
# 五、默认沙箱
## 5.1 通过jvm参数启用沙箱
`java -Djava.security.manager <other args>`
## 5.2 通过java代码启用沙箱
System.setSecurityManager(new SecurityManager());
## 5.3 指定策略文件
沙箱启动后，安全管理器会使用两个默认的策略文件来确定沙箱启动参数。当然也可以通过命令指定：
`java -Djava.security.policy=<URL>`
如果要求启动时只遵循一个策略文件，那么启动参数要加个等号，如下：
`java -Djava.security.policy==<URL>`
# 六、自定义安全管理器实现沙箱
通过策略文件实现沙箱，存在两个问题：
* 策略文件内容错误不能得到提示；
* 策略文件不够灵活；
* 策略文件方式默认加载的类有退出虚拟机的权限；

所以我们需要自己实现沙箱：
* 如果系统没有启用安全管理器，则我们启用一个具有所有权限的安全管理器。
如果系统已经启用安全管理器，则直接用当前的安全管理器。
* 提供一个通过沙箱运行代码的方法，默认没有任何权限，所有权限都需要指定。

# 七、参考资料
https://docs.oracle.com/javase/7/docs/technotes/guides/security/PolicyFiles.html#ChangingDefault

https://developer.aliyun.com/article/8620

https://stackoverflow.com/questions/1715036/how-do-i-create-a-java-sandbox

https://www.cnblogs.com/MyStringIsNotNull/p/8268351.html

https://developer.aliyun.com/article/8620


# 附录1：沙箱实现代码
```
package org.example;

import java.io.FilePermission;
import java.io.IOException;
import java.security.*;
import java.util.*;

/**
 * 设置一个安全管理器，对所有代码权限直接通过。
 * 然后提供一个confine方法，在指定的权限中运行代码。
 * */
public final class Sandbox {
    static {
        if (System.getSecurityManager() == null) {
            // Before installing the security manager, configure a decent ("positive") policy.
            Policy.setPolicy(new Policy() {
                @Override
                public boolean
                implies(ProtectionDomain domain, Permission permission) {
                    return true;
                }
            });
            System.setSecurityManager(new SecurityManager());
        }
    }

    private final AccessControlContext accessControlContext;

    /**
     * @param permissions Will be applied on later calls to {@link #confine(PrivilegedAction)}}
     */
    public Sandbox(PermissionCollection permissions) {
        this.accessControlContext = new AccessControlContext(new ProtectionDomain[]{
                new ProtectionDomain(null, permissions)
        });
    }

    /**
     * Runs the given <var>action</var>, confined by the permissions configured through the {@link
     * #Sandbox(PermissionCollection) constructor}.
     *
     * @return The value returned by the <var>action</var>
     */
    public <R> R
    confine(PrivilegedAction<R> action) {
        return AccessController.doPrivileged(action, this.accessControlContext);
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        PermissionCollection pc = new Permissions();
        pc.add(new RuntimePermission("createClassLoader",null));
        pc.add(new RuntimePermission("accessDeclaredMembers",null));
        pc.add(new FilePermission("/groovy/script/*","read"));
        //pc.add(new FilePermission("/-","read"));
        pc.add(new FilePermission(JarUtils.jarPath(Class.forName("groovy.lang.GroovyClassLoader")),"read"));
        pc.add(new PropertyPermission("groovy.compiler.strictNames","read"));
        PrivilegedAction pa = ()->{
            try {
                double r = Math.random();
                return GroovyScripts.exec("def run(variables){'hello'//"+r+"}",new HashMap<>());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
        Object o = new Sandbox(pc).confine(pa);
        new Sandbox(pc).confine(pa);
        System.out.println(o);
    }

}
```
# 附录2：默认策略文件内容
```
// Standard extensions get all permissions by default

grant codeBase "file:${{java.ext.dirs}}/*" {
        permission java.security.AllPermission;
};

// default permissions granted to all domains
grant {
        // Allows any thread to stop itself using the java.lang.Thread.stop()
        // method that takes no argument.
        // Note that this permission is granted by default only to remain
        // backwards compatible.
        // It is strongly recommended that you either remove this permission
        // from this policy file or further restrict it to code sources
        // that you specify, because Thread.stop() is potentially unsafe.
        // See the API specification of java.lang.Thread.stop() for more
        // information.
        permission java.lang.RuntimePermission "stopThread";

        // allows anyone to listen on dynamic ports
        permission java.net.SocketPermission "localhost:0", "listen";

        // "standard" properies that can be read by anyone
permission java.util.PropertyPermission "java.version", "read";
        permission java.util.PropertyPermission "java.vendor", "read";
        permission java.util.PropertyPermission "java.vendor.url", "read";
        permission java.util.PropertyPermission "java.class.version", "read";
        permission java.util.PropertyPermission "os.name", "read";
        permission java.util.PropertyPermission "os.version", "read";
        permission java.util.PropertyPermission "os.arch", "read";
        permission java.util.PropertyPermission "file.separator", "read";
        permission java.util.PropertyPermission "path.separator", "read";
        permission java.util.PropertyPermission "line.separator", "read";
permission java.util.PropertyPermission "java.specification.version", "read";
        permission java.util.PropertyPermission "java.specification.vendor", "read";
        permission java.util.PropertyPermission "java.specification.name", "read";

        permission java.util.PropertyPermission "java.vm.specification.version", "read";
        permission java.util.PropertyPermission "java.vm.specification.vendor", "read";
        permission java.util.PropertyPermission "java.vm.specification.name", "read";
        permission java.util.PropertyPermission "java.vm.version", "read";
        permission java.util.PropertyPermission "java.vm.vendor", "read";
        permission java.util.PropertyPermission "java.vm.name", "read";
};
```