package com.example.msg.service;

import com.example.msg.model.Msg;
import com.example.msg.repo.MsgRepo;
import com.example.msg.state.MsgEvents;
import com.example.msg.state.MsgStates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler;
import org.springframework.stereotype.Service;

@Service
public class MsgService{

    @Autowired
    ApplicationContext app;
    @Autowired
    MsgRepo msgRepo;

    PersistStateMachineHandler stateMachineHandler(){
        return app.getBean("msgPersistStateMachineHandler",PersistStateMachineHandler.class);
    }


    public void create(String content){
        Message<String> message = MessageBuilder.withPayload(MsgEvents.CREATE.name()).setHeader("msg",new Msg(null,null,content)).build();
        stateMachineHandler().handleEventWithStateReactively(message,MsgStates.S0.name()).subscribe();
    }
    public void send(Integer msgId) throws Exception {
        Msg msg = msgRepo.get(msgId);
        Message<String> message = MessageBuilder.withPayload(MsgEvents.SEND.name()).setHeader("msg",msg).build();
        stateMachineHandler().handleEventWithStateReactively(message,msg.state.name()).subscribe();
    }


}
