package com.example.global.service;

import com.example.global.state.Events;
import com.example.global.state.States;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MyService {

    @Resource(name = "myStateMachine")
    private StateMachine<States, Events> stateMachine;

    public void s1() {
        //stateMachine.sendEvent(MessageBuilder.withPayload(Events.E1).setHeader("time","2022-04-09").build());
        //stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(Events.E1).setHeader("time","2022-04-09").build()));
        stateMachine.sendEvent(Events.E1);
    }

    public void s2() {
        stateMachine.sendEvent(Events.E2);
    }
}
