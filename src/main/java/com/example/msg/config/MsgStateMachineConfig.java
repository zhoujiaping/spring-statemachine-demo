package com.example.msg.config;

import com.example.msg.model.Msg;
import com.example.msg.repo.MsgRepo;
import com.example.msg.state.MsgEvents;
import com.example.msg.state.MsgStates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.recipes.persist.PersistStateMachineHandler;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

import java.util.EnumSet;
import java.util.stream.Collectors;

@Component
@Configuration
public class MsgStateMachineConfig {


    @Bean
    //@Scope("prototype")
    public StateMachine<String, String> msgStateMachine(MsgRepo msgRepo) throws Exception {
        StateMachineBuilder.Builder<String, String> builder = StateMachineBuilder.builder();
        builder.configureConfiguration()
                .withConfiguration()
                .autoStartup(true)
                .machineId("stateMachine-msg")
                //.beanFactory(beanFactory)
                .listener(msgStateListener());

        builder.configureStates()
                .withStates()
                .initial(MsgStates.S0.name())
                //.end(MsgStates.SENT)
                .states(EnumSet.allOf(MsgStates.class).stream().map(it->it.name()).collect(Collectors.toSet()));
        builder.configureTransitions()
                .withExternal()
                .source(MsgStates.S0.name()).target(MsgStates.INIT.name()).event(MsgEvents.CREATE.name()).action(ctx -> {
                    System.out.println("action: CREATE");
                    Msg msg = (Msg) ctx.getMessageHeader("msg");
                    msg.state = MsgStates.valueOf(MsgStates.INIT.name());
                    msgRepo.save(msg);
                    //msgRepo.put(++counter, new Msg(counter, MsgStates.INIT, ctx.getMessageHeader("content").toString()));
                })
                .and()
                .withExternal()
                .source(MsgStates.INIT.name()).target(MsgStates.SENT.name()).event(MsgEvents.SEND.name()).action(ctx -> {
                    System.out.println("action: SEND");
                    Msg msg = (Msg) ctx.getMessageHeader("msg");
                    msg.state = MsgStates.valueOf(MsgStates.SENT.name());
                    msgRepo.save(msg);
                });
        return builder.build();
    }

    @Bean
    public StateMachineListener<String, String> msgStateListener() {
        return new StateMachineListenerAdapter<String, String>() {
            @Override
            public void stateChanged(State<String, String> from, State<String, String> to) {
                System.out.println("msgStateMachine: State change to " + to.getId());
            }
        };
    }

    @Bean
    //@Scope("prototype")
    public PersistStateMachineHandler msgPersistStateMachineHandler(MsgRepo msgRepo) throws Exception {
        PersistStateMachineHandler handler = new PersistStateMachineHandler(msgStateMachine(msgRepo));
        handler.addPersistStateChangeListener(localPersistStateChangeListener());
        return handler;
    }

    @Bean
    public LocalPersistStateChangeListener localPersistStateChangeListener(){
        return new LocalPersistStateChangeListener();
    }
    private class LocalPersistStateChangeListener implements PersistStateMachineHandler.PersistStateChangeListener {

        @Override
        public void onPersist(State<String, String> state, Message<String> message,
                              Transition<String, String> transition, StateMachine<String, String> stateMachine) {
            System.out.println("stateChanged: currentState= "+state.getId()+", action= "+message.getPayload() + ", headers= "+message.getHeaders());
        }
    }
}