package com.example.msg.repo;

import com.example.msg.model.Msg;
import com.example.msg.state.MsgStates;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Repository
public class MsgRepo {
    Integer counter;
    Map<Integer, Msg> msgMap = new HashMap<>();

    public Map<Integer, Msg> getMsgMap() {
        return msgMap;
    }

    @PostConstruct
    public void init() {
        msgMap.put(1, new Msg(1, MsgStates.INIT, "hello"));
        msgMap.put(2, new Msg(2, MsgStates.SENT, "hi"));
        counter = msgMap.size();
    }

    public void save(Msg msg) {
        if(msg.id==null){
            msg.id = ++counter;
        }
        msgMap.put(msg.id, msg);
    }

    public Msg get(Integer id) {
       return msgMap.get(id);
    }
}
