package com.example.global.cmd;

import com.example.global.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyCommandRunner implements CommandLineRunner {

    @Autowired
    private MyService myService;

    @Override
    public void run(String... args) throws InterruptedException {
        myService.s2();
        myService.s1();
        myService.s2();
    }
}
